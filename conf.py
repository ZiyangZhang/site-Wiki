# -*- coding: utf-8 -*-
"""Configuration for Wiki
"""

# For Maverick
site_prefix = "https://wiki.zhangziyang.site/"
source_dir = "../src/"
build_dir = "../dist/"
template = {
    "name": "Kepler",
    "type": "local",
    "path": "../Kepler"
}
index_page_size = 10
archives_page_size = 20
enable_jsdelivr = {
    "enabled": True,
    "repo": "bananans/site-Wiki@gh-pages"
}
category_by_folder = True
for_manual_build_trigger = 1

# 站点设置
site_name = "GlassySky 's Wiki"
site_logo = "${static_prefix}favicon.ico"
site_build_date = "2017-06-29T12:00+08:00"
author = "GlassySky"
email = "3084561637@qq.com"
author_homepage = "https://zhangziyang.site"
description = "GlassySky的个人Wiki"
key_words = ['Maverick', 'Glassysky', 'Kepler', 'wiki']
language = 'zh-CN'

valine = {
    "enable": True,
    "el": '#vcomments',
    "appId": "Fz5VQ9BPs6sJ3ip9MSnoxzLD-MdYXbMMI",
    "appKey": "atXS4KC6hTwUL9mbvpMosv5D",
    "visitor": True,
    "recordIP": False,
    "placeholder": "请不吝赐教"
}

external_links = [
    {
        "name": "TRIPLE NULL",
        "url": "https://www.imalan.cn",
        "brief": "三是虚指。至于是哪三无，我唔知。"
    },
    {
        "name": "BLOG",
        "url": "https://blog.imalan.cn",
        "brief": "熊猫小A的博客。隶属于「三无计划」。"
    },
    {
        "name": "LAB",
        "url": "https://lab.imalan.cn",
        "brief": "熊猫小A的实验室。隶属于「三无计划」。"
    },
    {
        "name": "GITHUB",
        "url": "https://github.com/AlanDecode",
        "brief": "My GitHub"
    },
    {
        "name": "CHANNEL",
        "url": "https://t.me/triple_null",
        "brief": "熊猫小A的广播。隶属于「三无计划」。"
    }
]
nav = [
    {
        "name": "HOME",
        "url": "${site_prefix}",
        "target": "_self"
    },
    {
        "name": "ARCHIVES",
        "url": "${site_prefix}archives/",
        "target": "_self"
    },
    {
        "name": "ABOUT",
        "url": "${site_prefix}about/",
        "target": "_self"
    }
]

social_links = [
    {
        "name": "Twitter",
        "url": "https://twitter.com/AlanDecode",
        "icon": "gi gi-twitter"
    },
    {
        "name": "GitHub",
        "url": "https://github.com/AlanDecode",
        "icon": "gi gi-github"
    },
    {
        "name": "Weibo",
        "url": "https://weibo.com/5245109677/",
        "icon": "gi gi-weibo"
    }
]

head_addon = r'''
<meta http-equiv="x-dns-prefetch-control" content="on">
<link rel="dns-prefetch" href="//cdn.jsdelivr.net" />
<link rel="stylesheet" href="${static_prefix}brand_font/embed.css" />
<style>.brand{font-family:FZCuJinLFW,serif;font-weight: normal!important;}</style>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon" sizes="180x180" href="${static_prefix}apple-touch-icon.png?v=PY43YeeEKx">
<link rel="icon" type="image/png" sizes="32x32" href="${static_prefix}favicon.png?v=yyLyaqbyRG">
<link rel="icon" type="image/png" sizes="16x16" href="${static_prefix}favicon.png?v=yyLyaqbyRG">
<link rel="mask-icon" href="${static_prefix}safari-pinned-tab.svg?v=yyLyaqbyRG" color="#505050">
<link rel="shortcut icon" href="${static_prefix}favicon.ico?v=yyLyaqbyRG">
<meta name="application-name" content="blog">
<meta name="apple-mobile-web-app-title" content="blog">
<meta name="msapplication-TileColor" content="#000000">
<meta name="theme-color" content="#000000">
<link rel="icon" type="image/png" sizes="32x32" href="${static_prefix}favicon.ico">
<script src="//instant.page/3.0.0" type="module" defer integrity="sha384-OeDn4XE77tdHo8pGtE1apMPmAipjoxUQ++eeJa6EtJCfHlvijigWiJpD7VDPWXV1"></script>
'''

footer_addon = r'''
<a no-style href="https://www.upyun.com" target="_blank">又拍云</a>
'''

body_addon = r'''
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?84aa17e8b54b3a478610b2a877640003";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
'''