---
layout: post
title: 使用removebg抠图
date: 2020-3-16
author: GlassySky
toc: true
categories: 
  - 日常技巧
tags:
  - 图片处理
---
# 使用

## 安装

在网站上注册获取 API 后一行代码安装抠图库：

```bash
pip install removebg
```
## 代码版本

### 单张抠图

接下来只需要三行代码就可以完成一张抠图：

```python
from removebg import RemoveBg
rmbg = RemoveBg("WPZ2Q4fraseKfAN9PPxxxxxx", "error.log") # 引号内是你获取的API
rmbg.remove_background_from_img_file("C:/Users/sony/Desktop/1.jpg") #图片地址
```
### 批量抠图

不仅能抠一张还可以批量抠图（把图片放到一个文件夹就可以了）：

```python
from removebg import RemoveBg
import os

rmbg = RemoveBg("WPZ2Q4fraseKfAN9PPxxxxxx", "error.log")
path = '%s/picture'%os.getcwd() #图片放到程序的同级文件夹 picture 里面
for pic in os.listdir(path):
    rmbg.remove_background_from_img_file("%s\%s"%(path,pic))
```
默认生成的图片格式尺寸是标准的，每月最多免费处理 50 张照片。如果想生成高清甚至 4K 或者处理更多图片需要付费。

>GitHub 库地址：https://github.com/brilam/remove-bg

## 在线抠图

除了代码还可以到官网在线抠图，只需要上传照片即可，不限图片处理次数：

![01](assets/01.jpg)扣完图可以马上换颜色或者背景：

![02](assets/02.gif)

## 软件版本

除了在线版还可以下载软件到本地使用，支持 Windows / Mac / Linux 三平台。

