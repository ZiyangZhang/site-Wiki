---
layout: post
title: Python以主程序的形式执行
date: 2020-03-13
author: GlassySky
toc: true
categories: 
  - 编程开发
  - Python
tags:
  - 模块
  - 包
---
# 使用

把你要执行的代码放入一个函数中，并创建一个if语句，在if语句里执行该函数。如下面的代码：
```python
def main():
    print("人生苦短，我用Python")
if _name_=='_main_':
    main()
```
# 原理
在每个模块的定义中都包括一个记录模块名称的变量`_name_`，程序可以检查该变量，以确定它们在哪个模块中执行。如果一个模块不是被导入其他程序中执行，那么它可能在解释器的顶级模块中执行。顶级模块的`_name_`变量的值为`_main_`。